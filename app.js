require('dotenv').config();

const Snoowrap = require('snoowrap');
const Snoostorm = require('snoostorm');
const fs = require('fs');

const r = new Snoowrap({
  userAgent: 'reddit-bot-example-node',
  clientId: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
  username: process.env.REDDIT_USER,
  password: process.env.REDDIT_PASS
});

const client = new Snoostorm(r);
const cardsContent = fs.readFileSync('cards.json');
const jsonContent = JSON.parse(cardsContent);

const streamOpts = {
  subreddit: 'hearthstone',
  results: 25
};

console.log('ready...');
const comments = client.CommentStream(streamOpts);

comments.on('comment', (comment) => {
  if (comment.body.startsWith('!flavor ')) {
    const cardName = comment.body.substr(8);
    const result = jsonContent.filter(card => card.name === cardName);
    if (result.length > 0) {
      comment.reply(result[0].flavor);
      console.log('Welcome to FlavorTown!!!');
    }
  }
});